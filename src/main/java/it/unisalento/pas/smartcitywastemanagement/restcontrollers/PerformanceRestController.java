

package it.unisalento.pas.smartcitywastemanagement.restcontrollers;



import it.unisalento.pas.smartcitywastemanagement.domain.Performance;
import it.unisalento.pas.smartcitywastemanagement.domain.PerformanceComune;
import it.unisalento.pas.smartcitywastemanagement.dto.*;
import it.unisalento.pas.smartcitywastemanagement.repositories.PerformanceComuneRepository;
import it.unisalento.pas.smartcitywastemanagement.repositories.PerformanceRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.*;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.Month;
import java.time.YearMonth;
import java.util.*;
import java.util.stream.Collectors;
import static it.unisalento.pas.smartcitywastemanagement.security.Scheduling.*;


@CrossOrigin
@RestController
@RequestMapping("/api/performances")
public class PerformanceRestController {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private PerformanceComuneRepository performanceComuneRepository;


    @Autowired
    private PerformanceRepository performanceRepository;

    @Scheduled(cron = "0 0 0 1 * ?")
    public void calculateMonthlyPerformanceAutomatically() {
        LocalDate today = LocalDate.now();
        LocalDate startDate = today.withDayOfMonth(1).minusMonths(1);
        LocalDate endDate = startDate.withDayOfMonth(startDate.lengthOfMonth());

        // Prepara il body della richiesta per l'autenticazione
        LoginDTO loginDTO = new LoginDTO();
        loginDTO.setUsername(username); // Sostituisci con l'username effettivo
        loginDTO.setPassword(password); // Sostituisci con la password effettiva

        // Imposta l'URL dell'endpoint di autenticazione
        //String authUrl = "http://my-sba-login:8080/api/users/authenticate";
        String authUrl = "https://dfaxe8nxs4.execute-api.us-east-1.amazonaws.com/dev/users/authenticate";


        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<LoginDTO> entity = new HttpEntity<>(loginDTO, headers);

        try {
            // Effettua la chiamata POST per ottenere il token
            ResponseEntity<AuthenticationResponseDTO> response = restTemplate.postForEntity(authUrl, entity, AuthenticationResponseDTO.class);

            if (response.getStatusCode().equals(HttpStatus.OK) && response.getBody() != null) {
                String authToken = response.getBody().getJwt();
                System.out.println("Token JWT ottenuto per lo scheduling: " + authToken);


                calculatePerformanceInternal(startDate, endDate, authToken);
            } else {
                System.err.println("Errore durante l'autenticazione. Status: " + response.getStatusCode());
            }
        } catch (Exception e) {
            System.err.println("Errore durante la chiamata all'API di autenticazione: " + e.getMessage());
        }
    }


    @PreAuthorize("hasAnyRole('COMUNE', 'ADMIN')")
    @PostMapping("/calculatePerformance")
    public ResponseEntity<?> calculatePerformance(
            @RequestHeader("Authorization") String authToken,
            @RequestBody PerformanceCalculationRequestDTO request) {
        return calculatePerformanceInternal(request.getStartDate(), request.getEndDate(), authToken);


    }

    private ResponseEntity<?> calculatePerformanceInternal(LocalDate startDate, LocalDate endDate, String authToken) {
        List<Double> performanceIndices = new ArrayList<>();
        List<PerformanceResultDTO> performanceResults = new ArrayList<>();


        HttpHeaders headers = new HttpHeaders();
        headers.set("Authorization", authToken.startsWith("Bearer ") ? authToken : "Bearer " + authToken);
        HttpEntity<String> entity = new HttpEntity<>(headers);

        // Prima chiamata API per ottenere le misure dei cassonetti
        //String measurementsUrl = String.format("http://my-sba-allarmi:8080/api/measurements/filterByMonthAndYear?startDate=%s&endDate=%s", startDate, endDate);
        String measurementsUrl = String.format("https://64xqhz017j.execute-api.us-east-1.amazonaws.com/dev/measurements/filterByMonthAndYear?startDate=%s&endDate=%s", startDate, endDate);
        ResponseEntity<List<MeasureWasteBinDTO>> measurementsResponse = restTemplate.exchange(
                measurementsUrl,
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<List<MeasureWasteBinDTO>>() {
                });

        if (!measurementsResponse.getStatusCode().is2xxSuccessful() || measurementsResponse.getBody() == null) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Errore durante la chiamata al servizio delle misurazioni");
        }

        List<MeasureWasteBinDTO> measures = measurementsResponse.getBody();
        Set<String> binIds = measures.stream().map(MeasureWasteBinDTO::getIdBin).collect(Collectors.toSet());

        // Seconda chiamata API per ottenere i dettagli sui cassonetti
        String wasteContainersUrl = "https://isgjf4a2cc.execute-api.us-east-1.amazonaws.com/dev/waste-containers/batch";
        //String wasteContainersUrl = "http://my-sba-cassonetti:8080/api/waste-containers/batch";
        HttpEntity<Set<String>> containersRequestEntity = new HttpEntity<>(binIds, headers);
        ResponseEntity<List<WasteContainerDTO>> containersResponse = restTemplate.exchange(
                wasteContainersUrl,
                HttpMethod.POST,
                containersRequestEntity,
                new ParameterizedTypeReference<List<WasteContainerDTO>>() {
                });

        if (!containersResponse.getStatusCode().is2xxSuccessful() || containersResponse.getBody() == null) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("Errore durante la chiamata al servizio dei cassonetti");
        }

        List<WasteContainerDTO> containers = containersResponse.getBody();

        // Combina i risultati delle due chiamate API in un'unica risposta aggregata
        Map<String, Object> aggregatedResponse = new HashMap<>();
        aggregatedResponse.put("measurements", measures); // Misurazioni ottenute dalla prima chiamata API
        aggregatedResponse.put("containerDetails", containers); // Dettagli dei cassonetti ottenuti dalla seconda chiamata API

        List<MeasureWasteBinDTO> filteredMeasures = measures.stream()
                .filter(measure -> measure.getCitizen() != null) // Escludi misure con citizen null
                .collect(Collectors.toList());

        // Usa le misure filtrate per l'aggregazione per cittadino
        Map<String, List<MeasureWasteBinDTO>> measuresByCitizen = filteredMeasures.stream()
                .collect(Collectors.groupingBy(MeasureWasteBinDTO::getCitizen));

        // Mappa per identificare rapidamente il tipo di cassonetto per ID
        Map<String, String> binTypeById = containers.stream()
                .filter(container -> container.getId() != null && container.getType() != null) // Escludi valori null
                .collect(Collectors.toMap(WasteContainerDTO::getId, WasteContainerDTO::getType));



        double Pindifferenziato = 1.3;


        double PinPlastica = 0.8;
        double PinOrganico = 0.9;
        double PinCartone = 0.7;
        double PinVetroEMetalli = 1.1;

// Mappa per calcolare gli indici di inquinamento specifici per ogni tipo di rifiuto e per cittadino

        Map<String, Double> performanceIndexByCitizen = new HashMap<>();


        measuresByCitizen.forEach((citizenId, citizenMeasures) -> {
            // Inizializza mappa per tenere traccia dei sumWasteDisposal per tipo di rifiuto
            Map<String, Double> sumWasteDisposalByType = new HashMap<>();
            sumWasteDisposalByType.put("Unsorted waste", 0.0);
            sumWasteDisposalByType.put("Plastic", 0.0);
            sumWasteDisposalByType.put("Organic", 0.0);
            sumWasteDisposalByType.put("GlassMetals", 0.0);
            sumWasteDisposalByType.put("Cardboard", 0.0);

            // Aggrega i waste_disposal per tipo di rifiuto
            citizenMeasures.forEach(measure -> {
                String binType = binTypeById.get(measure.getIdBin());
                double wasteDisposal = measure.getWaste_disposal();
                sumWasteDisposalByType.merge(binType, wasteDisposal, Double::sum);
            });

            // Calcola l'indice di inquinamento per ogni tipo di rifiuto e lo assegna al cittadino
            Map<String, Double> pollutionIndexByType = new HashMap<>();
            pollutionIndexByType.put("Unsorted waste", sumWasteDisposalByType.get("Unsorted waste") * Pindifferenziato);
            pollutionIndexByType.put("Plastic", sumWasteDisposalByType.get("Plastic") * PinPlastica);
            pollutionIndexByType.put("Organic", sumWasteDisposalByType.get("Organic") * PinOrganico);
            pollutionIndexByType.put("GlassMetals", sumWasteDisposalByType.get("GlassMetals") * PinVetroEMetalli);
            pollutionIndexByType.put("Cardboard", sumWasteDisposalByType.get("Cardboard") * PinCartone);

            double totalPollutionIndex = pollutionIndexByType.values().stream().mapToDouble(Double::doubleValue).sum();
            double unsortedWasteIndex = pollutionIndexByType.getOrDefault("Unsorted waste", 0.0);

            // Calcola il rapporto di inquinamento solo se il totale degli indici di inquinamento è maggiore di zero per evitare divisione per zero
            double pollutionRatio = totalPollutionIndex > 0 ? unsortedWasteIndex / totalPollutionIndex : 0;
            double performanceIndex = (1 - pollutionRatio) * 100;
            BigDecimal performanceIndexRounded = BigDecimal.valueOf(performanceIndex).setScale(2, RoundingMode.HALF_UP);
            PerformanceResultDTO result = new PerformanceResultDTO(
                    citizenId,
                    startDate.getMonth().toString(),
                    startDate.getYear(),
                    performanceIndexRounded.doubleValue()
            );
            performanceResults.add(result);

            performanceIndices.add(performanceIndexRounded.doubleValue());

            Optional<Performance> existingPerformanceOpt = performanceRepository.findByIdCittadino(citizenId);

            Performance performance;
            Performance.PerformanceAnnuale performanceAnnuale = null;
            Performance.MesePerformance mesePerformance = new Performance.MesePerformance();
            mesePerformance.setMese(startDate.getMonth().toString());
            mesePerformance.setIndicePerformance(performanceIndexRounded.doubleValue());

            if (existingPerformanceOpt.isPresent()) {
                // Se esiste, aggiorna l'oggetto esistente
                performance = existingPerformanceOpt.get();

                // Cerca l'anno corrente nell'elenco delle performance annuali
                Optional<Performance.PerformanceAnnuale> existingPerformanceAnnuale = performance.getPerformanceAnnuale().stream()
                        .filter(pa -> pa.getAnno() == startDate.getYear())
                        .findFirst();

                if (existingPerformanceAnnuale.isPresent()) {
                    performanceAnnuale = existingPerformanceAnnuale.get();
                } else {
                    // Se l'anno non è presente, creane uno nuovo e aggiungilo all'elenco
                    performanceAnnuale = new Performance.PerformanceAnnuale();
                    performanceAnnuale.setAnno(startDate.getYear());
                    performance.getPerformanceAnnuale().add(performanceAnnuale);
                }
            } else {
                // Se non esiste, crea un nuovo oggetto Performance
                performance = new Performance();
                performance.setIdCittadino(citizenId);

                performanceAnnuale = new Performance.PerformanceAnnuale();
                performanceAnnuale.setAnno(startDate.getYear());
                performance.setPerformanceAnnuale(new ArrayList<>());
                performance.getPerformanceAnnuale().add(performanceAnnuale);
            }

            // Verifica e aggiorna o aggiungi la performance mensile
            if (performanceAnnuale.getPerformanceMensili() == null) {
                performanceAnnuale.setPerformanceMensili(new ArrayList<>());
            }

            Optional<Performance.MesePerformance> existingMesePerformance = performanceAnnuale.getPerformanceMensili().stream()
                    .filter(mp -> mp.getMese().equals(mesePerformance.getMese()))
                    .findFirst();

            if (existingMesePerformance.isPresent()) {
                // Se il mese è già presente, aggiorna l'indice di performance
                existingMesePerformance.get().setIndicePerformance(mesePerformance.getIndicePerformance());
            } else {
                // Se il mese non è presente, aggiungi la nuova performance mensile
                performanceAnnuale.getPerformanceMensili().add(mesePerformance);
            }

            // Salva l'oggetto Performance aggiornato nel database
            performanceRepository.save(performance);
        });
        double median = calculateMedian(performanceIndices);
        saveOrUpdatePerformanceComune(startDate.getMonth().toString(), startDate.getYear(), median);



// Restituisce la risposta aggregata al client con dettaglio per tipo di rifiuto
        return ResponseEntity.ok().body(performanceResults);


    }

    private double calculateMedian(List<Double> values) {
        Collections.sort(values);
        if (values.size() % 2 == 0) {
            return (values.get(values.size() / 2 - 1) + values.get(values.size() / 2)) / 2.0;
        } else {
            return values.get(values.size() / 2);
        }
    }


    private void saveOrUpdatePerformanceComune(String month, int year, double median) {
        // Trova il documento PerformanceComune esistente o crea uno nuovo
        PerformanceComune performanceComune = performanceComuneRepository.findAll().stream()
                .findFirst()
                .orElseGet(() -> {
                    PerformanceComune newPerformanceComune = new PerformanceComune();
                    newPerformanceComune.setPerformanceAnnuale(new ArrayList<>()); // Inizializza la lista qui
                    return newPerformanceComune;
                });

        // Trova o crea l'oggetto PerformanceAnnualeComune per l'anno specificato
        PerformanceComune.PerformanceAnnualeComune performanceAnnuale = performanceComune.getPerformanceAnnuale().stream()
                .filter(pa -> pa.getAnno() == year)
                .findFirst()
                .orElseGet(() -> {
                    PerformanceComune.PerformanceAnnualeComune newPerformanceAnnuale = new PerformanceComune.PerformanceAnnualeComune();
                    newPerformanceAnnuale.setPerformanceMensili(new ArrayList<>());
                    return newPerformanceAnnuale;
                });

        if (performanceAnnuale.getAnno() == 0) { // Se è un nuovo oggetto PerformanceAnnualeComune
            performanceAnnuale.setAnno(year);
            performanceComune.getPerformanceAnnuale().add(performanceAnnuale);
        }

        // Trova o crea l'oggetto MesePerformanceComune per il mese specificato
        PerformanceComune.MesePerformanceComune mesePerformance = performanceAnnuale.getPerformanceMensili().stream()
                .filter(mp -> mp.getMese().equals(month))
                .findFirst()
                .orElseGet(() -> new PerformanceComune.MesePerformanceComune());

        if (mesePerformance.getMese() == null) { // Se è un nuovo oggetto MesePerformanceComune
            mesePerformance.setMese(month);
            performanceAnnuale.getPerformanceMensili().add(mesePerformance);
        }

        // Aggiorna l'indice di performance per il mese specificato
        mesePerformance.setIndicePerformance(median);

        // Salva l'oggetto aggiornato (o nuovo) nel database
        performanceComuneRepository.save(performanceComune);
    }



    @PreAuthorize("hasAnyRole('COMUNE', 'ADMIN')")
    @PostMapping("/getLatestPerformanceForCitizens")
    public ResponseEntity<List<PerformanceDTO>> getLatestPerformanceForCitizens(@RequestBody PerformanceQueryDTO queryDTO) {
        // Ottiene tutte le performance per gli idCittadini forniti
        List<Performance> performances = performanceRepository.findAllByIdCittadini(queryDTO.getIdCittadini());

        // Conversione delle entità Performance recuperate in PerformanceDTO, considerando solo l'ultima performance
        List<PerformanceDTO> performanceDTOs = performances.stream().map(performance -> {
            PerformanceDTO dto = new PerformanceDTO();
            dto.setIdCittadino(performance.getIdCittadino());

            // Trova l'anno più recente e il mese più recente per quel cittadino
            Performance.PerformanceAnnuale latestYear = performance.getPerformanceAnnuale().stream()
                    .max(Comparator.comparingInt(Performance.PerformanceAnnuale::getAnno))
                    .orElse(null);

            if (latestYear != null) {
                Performance.MesePerformance latestMonth = latestYear.getPerformanceMensili().stream()
                        .max(Comparator.comparing(mesePerformance -> YearMonth.of(latestYear.getAnno(), Month.valueOf(mesePerformance.getMese().toUpperCase()))))
                        .orElse(null);

                if (latestMonth != null) {
                    // Crea un PerformanceAnnualeDTO e un MesePerformanceDTO per l'ultima performance
                    PerformanceDTO.PerformanceAnnualeDTO annualeDTO = new PerformanceDTO.PerformanceAnnualeDTO();
                    annualeDTO.setAnno(latestYear.getAnno());
                    PerformanceDTO.MesePerformanceDTO meseDTO = new PerformanceDTO.MesePerformanceDTO(latestMonth.getMese(), latestMonth.getIndicePerformance());
                    annualeDTO.setPerformanceMensili(Collections.singletonList(meseDTO));

                    dto.setPerformanceAnnuale(Collections.singletonList(annualeDTO));
                }
            }

            return dto;
        }).collect(Collectors.toList());

        return ResponseEntity.ok(performanceDTOs);
    }

    @PreAuthorize("hasAnyRole('COMUNE', 'ADMIN', 'CITIZEN')")
    @GetMapping("/getLatestPerformanceByCitizen") //prima era byCitizenAndYear
    public ResponseEntity<PerformanceDTO> getLatestPerformanceByCitizen(
            @RequestParam("idCittadino") String idCittadino) {

        Optional<Performance> foundPerformance = performanceRepository.findByIdCittadino(idCittadino);

        if (foundPerformance.isPresent()) {
            Performance performance = foundPerformance.get();

            // Trova l'ultimo anno di performance disponibile
            Optional<Performance.PerformanceAnnuale> foundAnnuale = performance.getPerformanceAnnuale().stream()
                    .max(Comparator.comparingInt(Performance.PerformanceAnnuale::getAnno));

            if (foundAnnuale.isPresent()) {
                PerformanceDTO performanceDTO = new PerformanceDTO();
                performanceDTO.setIdCittadino(idCittadino);

                PerformanceDTO.PerformanceAnnualeDTO performanceAnnualeDTO = new PerformanceDTO.PerformanceAnnualeDTO();
                performanceAnnualeDTO.setAnno(foundAnnuale.get().getAnno());

                List<PerformanceDTO.MesePerformanceDTO> performanceMensiliDTO = foundAnnuale.get().getPerformanceMensili().stream()
                        .map(mesePerformance -> new PerformanceDTO.MesePerformanceDTO(mesePerformance.getMese(), mesePerformance.getIndicePerformance()))
                        .collect(Collectors.toList());

                performanceAnnualeDTO.setPerformanceMensili(performanceMensiliDTO);
                performanceDTO.setPerformanceAnnuale(Collections.singletonList(performanceAnnualeDTO));

                return ResponseEntity.ok(performanceDTO);
            }
        }
        return ResponseEntity.notFound().build();
    }



    @PreAuthorize("hasAnyRole('COMUNE', 'ADMIN','SCHEDULING')")
    @GetMapping("/getPerformancesByYear")
    public ResponseEntity<List<PerformanceDTO>> getPerformanceByYear(@RequestParam("anno") int anno) {
        List<Performance> allPerformances = performanceRepository.findAll();
        List<PerformanceDTO> result = new ArrayList<>();

        for (Performance performance : allPerformances) {
            PerformanceDTO dto = new PerformanceDTO();
            dto.setIdCittadino(performance.getIdCittadino());

            List<PerformanceDTO.PerformanceAnnualeDTO> performanceAnnualeDTOList = performance.getPerformanceAnnuale().stream()
                    .filter(performanceAnnuale -> performanceAnnuale.getAnno() == anno)
                    .map(performanceAnnuale -> {
                        PerformanceDTO.PerformanceAnnualeDTO performanceAnnualeDTO = new PerformanceDTO.PerformanceAnnualeDTO();
                        performanceAnnualeDTO.setAnno(performanceAnnuale.getAnno());

                        List<PerformanceDTO.MesePerformanceDTO> performanceMensiliDTOList = performanceAnnuale.getPerformanceMensili().stream()
                                .map(mesePerformance -> new PerformanceDTO.MesePerformanceDTO(mesePerformance.getMese(), mesePerformance.getIndicePerformance()))
                                .collect(Collectors.toList());

                        performanceAnnualeDTO.setPerformanceMensili(performanceMensiliDTOList);
                        return performanceAnnualeDTO;
                    })
                    .collect(Collectors.toList());

            if (!performanceAnnualeDTOList.isEmpty()) {
                dto.setPerformanceAnnuale(performanceAnnualeDTOList);
                result.add(dto);
            }
        }

        if (result.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        return ResponseEntity.ok(result);
    }

    @PreAuthorize("hasAnyRole('COMUNE', 'ADMIN')")
    @GetMapping("/getLatestPerformanceForAllCitizens")
    public ResponseEntity<List<PerformanceDTO>> getLatestPerformanceForAllCitizens() {
        List<Performance> allPerformances = performanceRepository.findAll();

        List<PerformanceDTO> performanceDTOs = allPerformances.stream().map(performance -> {
            PerformanceDTO dto = new PerformanceDTO();
            dto.setIdCittadino(performance.getIdCittadino());

            Performance.PerformanceAnnuale latestYear = performance.getPerformanceAnnuale().stream()
                    .max(Comparator.comparingInt(Performance.PerformanceAnnuale::getAnno))
                    .orElse(null);

            Performance.MesePerformance latestMonth = null;
            if (latestYear != null) {
                latestMonth = latestYear.getPerformanceMensili().stream()
                        .max(Comparator.comparing(mesePerformance -> Month.valueOf(mesePerformance.getMese().toUpperCase()).getValue()))
                        .orElse(null);
            }

            if (latestMonth != null) {
                PerformanceDTO.PerformanceAnnualeDTO annualeDTO = new PerformanceDTO.PerformanceAnnualeDTO();
                annualeDTO.setAnno(latestYear.getAnno());
                PerformanceDTO.MesePerformanceDTO meseDTO = new PerformanceDTO.MesePerformanceDTO(latestMonth.getMese(), latestMonth.getIndicePerformance());
                annualeDTO.setPerformanceMensili(List.of(meseDTO));

                dto.setPerformanceAnnuale(List.of(annualeDTO));
            }

            return dto;
        }).collect(Collectors.toList());

        return ResponseEntity.ok(performanceDTOs);
    }

    @PreAuthorize("hasAnyRole('COMUNE', 'ADMIN')")

    @GetMapping("/getLastYearPerformanceCityHall")
    public ResponseEntity<PerformanceComuneDTO> getLastYearPerformance() {
        List<PerformanceComune.PerformanceAnnualeComune> lastYearPerformanceList = performanceComuneRepository.findLastYearPerformance();

        if (lastYearPerformanceList.isEmpty()) {
            return ResponseEntity.notFound().build();
        }

        PerformanceComuneDTO performanceComuneDTO = new PerformanceComuneDTO();

        // Converti ogni PerformanceAnnualeComune in PerformanceAnnualeComuneDTO
        List<PerformanceComuneDTO.PerformanceAnnualeComuneDTO> performanceAnnualeDTOList = lastYearPerformanceList.stream()
                .map(performanceAnnuale -> {
                    PerformanceComuneDTO.PerformanceAnnualeComuneDTO performanceAnnualeDTO = new PerformanceComuneDTO.PerformanceAnnualeComuneDTO();
                    performanceAnnualeDTO.setAnno(performanceAnnuale.getAnno());

                    List<PerformanceComuneDTO.MesePerformanceComuneDTO> performanceMensiliDTOList = performanceAnnuale.getPerformanceMensili().stream()
                            .map(mesePerformance -> {
                                PerformanceComuneDTO.MesePerformanceComuneDTO mesePerformanceDTO = new PerformanceComuneDTO.MesePerformanceComuneDTO();
                                mesePerformanceDTO.setMese(mesePerformance.getMese());
                                mesePerformanceDTO.setIndicePerformance(mesePerformance.getIndicePerformance());
                                return mesePerformanceDTO;
                            })
                            .collect(Collectors.toList());

                    performanceAnnualeDTO.setPerformanceMensili(performanceMensiliDTOList);
                    return performanceAnnualeDTO;
                })
                .collect(Collectors.toList());

        performanceComuneDTO.setPerformanceAnnuale(performanceAnnualeDTOList);

        return ResponseEntity.ok(performanceComuneDTO);
    }
}

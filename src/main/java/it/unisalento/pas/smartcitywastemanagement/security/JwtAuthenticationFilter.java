package it.unisalento.pas.smartcitywastemanagement.security;


import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.security.SignatureException;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.Collections;

public class JwtAuthenticationFilter extends OncePerRequestFilter {

    @Autowired
    private JwtUtilities jwtUtilities;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
            throws ServletException, IOException {

        final String authorizationHeader = request.getHeader("Authorization");

        String jwt = null;
        try {
            if (authorizationHeader != null && authorizationHeader.startsWith("Bearer ")) {
                jwt = authorizationHeader.substring(7);
                System.out.println("Extracted JWT: " + jwt);

                if (jwtUtilities.validateToken(jwt)) {
                    String username = jwtUtilities.extractUsername(jwt);
                    String role = jwtUtilities.extractRole(jwt).toUpperCase(); // Trasforma il ruolo in maiuscolo

                    System.out.println("Username from JWT: " + username);
                    System.out.println("Role from JWT: " + role);

                    // Crea un'istanza di UsernamePasswordAuthenticationToken con il ruolo trasformato in maiuscolo
                    GrantedAuthority authority = new SimpleGrantedAuthority("ROLE_" + role);
                    UsernamePasswordAuthenticationToken authentication =
                            new UsernamePasswordAuthenticationToken(username, null, Collections.singletonList(authority));

                    SecurityContextHolder.getContext().setAuthentication(authentication);
                }
            }
        } catch (SignatureException e) {
            System.err.println("Firma errata");
            // Puoi anche restituire una risposta HTTP con un errore specifico, se necessario
        } catch (ExpiredJwtException e) {
            System.err.println("Token scaduto");
        } catch (MalformedJwtException e) {
            System.err.println("Token malformato");
        }

        chain.doFilter(request, response);
    }
}
package it.unisalento.pas.smartcitywastemanagement.repositories;

import it.unisalento.pas.smartcitywastemanagement.domain.PerformanceComune;
import org.springframework.data.mongodb.repository.Aggregation;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface PerformanceComuneRepository extends MongoRepository<PerformanceComune, String> {

    @Aggregation(pipeline = {
            "{ $unwind: '$performanceAnnuale' }",
            "{ $sort: { 'performanceAnnuale.anno': -1 } }",
            "{ $limit: 1 }",
            "{ $project: { _id: 0, 'anno': '$performanceAnnuale.anno', 'performanceMensili': '$performanceAnnuale.performanceMensili' } }"
    })
    List<PerformanceComune.PerformanceAnnualeComune> findLastYearPerformance();
}
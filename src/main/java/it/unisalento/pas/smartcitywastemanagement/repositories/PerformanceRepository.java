package it.unisalento.pas.smartcitywastemanagement.repositories;

        import it.unisalento.pas.smartcitywastemanagement.domain.Performance;
        import org.springframework.data.mongodb.repository.Aggregation;
        import org.springframework.data.mongodb.repository.MongoRepository;
        import org.springframework.data.mongodb.repository.Query;

        import java.util.List;
        import java.util.Optional;

public interface PerformanceRepository extends MongoRepository<Performance, String> {
        Optional<Performance> findByIdCittadino(String idCittadino);

        @Query("{ 'idCittadino': { $in: ?0 } }")
        List<Performance> findAllByIdCittadini(List<String> idCittadini);





}
package it.unisalento.pas.smartcitywastemanagement.dto;

public class PerformanceResultDTO {
    private String idCittadino;
    private String mese;
    private int anno;
    private double indicePerformance;

    // Costruttori, getter e setter
    public PerformanceResultDTO(String idCittadino, String mese, int anno, double indicePerformance) {
        this.idCittadino = idCittadino;
        this.mese = mese;
        this.anno = anno;
        this.indicePerformance = indicePerformance;
    }

    public String getIdCittadino() {
        return idCittadino;
    }

    public void setIdCittadino(String idCittadino) {
        this.idCittadino = idCittadino;
    }

    public String getMese() {
        return mese;
    }

    public void setMese(String mese) {
        this.mese = mese;
    }

    public int getAnno() {
        return anno;
    }

    public void setAnno(int anno) {
        this.anno = anno;
    }

    public double getIndicePerformance() {
        return indicePerformance;
    }

    public void setIndicePerformance(double indicePerformance) {
        this.indicePerformance = indicePerformance;
    }
}

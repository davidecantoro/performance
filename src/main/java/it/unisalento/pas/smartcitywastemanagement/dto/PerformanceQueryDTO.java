// Questo DTO viene utilizzato per accogliere i dati della richiesta POST


package it.unisalento.pas.smartcitywastemanagement.dto;

import java.util.List;

public class PerformanceQueryDTO {
    private List<String> idCittadini;

    // Getter e Setter
    public List<String> getIdCittadini() {
        return idCittadini;
    }

    public void setIdCittadini(List<String> idCittadini) {
        this.idCittadini = idCittadini;
    }


}

package it.unisalento.pas.smartcitywastemanagement.dto;

import java.util.List;

public class PerformanceComuneDTO {

    private List<PerformanceAnnualeComuneDTO> performanceAnnuale;

    public List<PerformanceAnnualeComuneDTO> getPerformanceAnnuale() {
        return performanceAnnuale;
    }

    public void setPerformanceAnnuale(List<PerformanceAnnualeComuneDTO> performanceAnnuale) {
        this.performanceAnnuale = performanceAnnuale;
    }

    public static class PerformanceAnnualeComuneDTO {
        private int anno;
        private List<MesePerformanceComuneDTO> performanceMensili;

        public int getAnno() {
            return anno;
        }

        public void setAnno(int anno) {
            this.anno = anno;
        }

        public List<MesePerformanceComuneDTO> getPerformanceMensili() {
            return performanceMensili;
        }

        public void setPerformanceMensili(List<MesePerformanceComuneDTO> performanceMensili) {
            this.performanceMensili = performanceMensili;
        }
    }

    public static class MesePerformanceComuneDTO {
        private String mese;
        private double indicePerformance;

        public String getMese() {
            return mese;
        }

        public void setMese(String mese) {
            this.mese = mese;
        }

        public double getIndicePerformance() {
            return indicePerformance;
        }

        public void setIndicePerformance(double indicePerformance) {
            this.indicePerformance = indicePerformance;
        }
    }
}

package it.unisalento.pas.smartcitywastemanagement.dto;

import java.util.Date;

public class MeasureWasteBinDTO {
    private String id;
    private Date timestamp;
    private String idBin;
    private Double fillingLevel;
    private String citizen;
    private Double waste_disposal;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public String getIdBin() {
        return idBin;
    }

    public void setIdBin(String idBin) {
        this.idBin = idBin;
    }

    public Double getFillingLevel() {
        return fillingLevel;
    }

    public void setFillingLevel(Double fillingLevel) {
        this.fillingLevel = fillingLevel;
    }
    public String getCitizen() {
        return citizen;
    }

    public void setCitizen(String citizen) {
        this.citizen = citizen;
    }

    public Double getWaste_disposal() {
        return waste_disposal;
    }

    public void setWaste_disposal(Double waste_disposal) {
        this.waste_disposal = waste_disposal;
    }


}

package it.unisalento.pas.smartcitywastemanagement.dto;

import java.util.List;

public class PerformanceDTO {

    private String idCittadino;
    private List<PerformanceAnnualeDTO> performanceAnnuale;

    public String getIdCittadino() {
        return idCittadino;
    }

    public void setIdCittadino(String idCittadino) {
        this.idCittadino = idCittadino;
    }

    public List<PerformanceAnnualeDTO> getPerformanceAnnuale() {
        return performanceAnnuale;
    }

    public void setPerformanceAnnuale(List<PerformanceAnnualeDTO> performanceAnnuale) {
        this.performanceAnnuale = performanceAnnuale;
    }

    public static class PerformanceAnnualeDTO {
        private int anno;
        private List<MesePerformanceDTO> performanceMensili;

        public int getAnno() {
            return anno;
        }

        public void setAnno(int anno) {
            this.anno = anno;
        }

        public List<MesePerformanceDTO> getPerformanceMensili() {
            return performanceMensili;
        }

        public void setPerformanceMensili(List<MesePerformanceDTO> performanceMensili) {
            this.performanceMensili = performanceMensili;
        }
    }

    public static class MesePerformanceDTO {
        private String mese;
        private double indicePerformance;
        public MesePerformanceDTO(String mese, double indicePerformance) {
            this.mese = mese;
            this.indicePerformance = indicePerformance;
        }

        public String getMese() {
            return mese;
        }

        public void setMese(String mese) {
            this.mese = mese;
        }

        public double getIndicePerformance() {
            return indicePerformance;
        }

        public void setIndicePerformance(double indicePerformance) {
            this.indicePerformance = indicePerformance;
        }
    }
}

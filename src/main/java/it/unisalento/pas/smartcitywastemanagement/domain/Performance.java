package it.unisalento.pas.smartcitywastemanagement.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import java.util.List;

@Document(collection = "performances")
public class Performance {

    @Id
    private String id;
    private String idCittadino;
    private List<PerformanceAnnuale> performanceAnnuale;

    // Getter and Setter
    public String getIdCittadino() {
        return idCittadino;
    }

    public void setIdCittadino(String idCittadino) {
        this.idCittadino = idCittadino;
    }

    public List<PerformanceAnnuale> getPerformanceAnnuale() {
        return performanceAnnuale;
    }

    public void setPerformanceAnnuale(List<PerformanceAnnuale> performanceAnnuale) {
        this.performanceAnnuale = performanceAnnuale;
    }

    public static class PerformanceAnnuale {
        private int anno;
        private List<MesePerformance> performanceMensili;

        // Getter and Setter
        public int getAnno() {
            return anno;
        }

        public void setAnno(int anno) {
            this.anno = anno;
        }

        public List<MesePerformance> getPerformanceMensili() {
            return performanceMensili;
        }

        public void setPerformanceMensili(List<MesePerformance> performanceMensili) {
            this.performanceMensili = performanceMensili;
        }
    }

    public static class MesePerformance {
        private String mese;
        private double indicePerformance;

        // Getter and Setter
        public String getMese() {
            return mese;
        }

        public void setMese(String mese) {
            this.mese = mese;
        }

        public double getIndicePerformance() {
            return indicePerformance;
        }

        public void setIndicePerformance(double indicePerformance) {
            this.indicePerformance = indicePerformance;
        }
    }
}

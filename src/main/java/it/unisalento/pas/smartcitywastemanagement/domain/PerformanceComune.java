package it.unisalento.pas.smartcitywastemanagement.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import java.util.List;

@Document(collection = "performanceComune")
public class PerformanceComune {

    @Id
    private String id;
    private List<PerformanceAnnualeComune> performanceAnnuale;

    // Getter and Setter
    public List<PerformanceAnnualeComune> getPerformanceAnnuale() {
        return performanceAnnuale;
    }

    public void setPerformanceAnnuale(List<PerformanceAnnualeComune> performanceAnnuale) {
        this.performanceAnnuale = performanceAnnuale;
    }

    public static class PerformanceAnnualeComune {
        private int anno;
        private List<MesePerformanceComune> performanceMensili;

        // Getter and Setter
        public int getAnno() {
            return anno;
        }

        public void setAnno(int anno) {
            this.anno = anno;
        }

        public List<MesePerformanceComune> getPerformanceMensili() {
            return performanceMensili;
        }

        public void setPerformanceMensili(List<MesePerformanceComune> performanceMensili) {
            this.performanceMensili = performanceMensili;
        }
    }

    public static class MesePerformanceComune {
        private String mese;
        private double indicePerformance;

        // Getter and Setter
        public String getMese() {
            return mese;
        }

        public void setMese(String mese) {
            this.mese = mese;
        }

        public double getIndicePerformance() {
            return indicePerformance;
        }

        public void setIndicePerformance(double indicePerformance) {
            this.indicePerformance = indicePerformance;
        }
    }
}
